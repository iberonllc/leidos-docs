# API Documentation

* [NciteVetting.Frontend.Leidos.RegistrationController](#ncitevettingfrontendleidosregistrationcontroller)
  * [create](#ncitevettingfrontendleidosregistrationcontrollercreate)

## NciteVetting.Frontend.Leidos.RegistrationController
### NciteVetting.Frontend.Leidos.RegistrationController.create
#### [V2] Registration (with a reject client) of a previously unregistered subject does not save the subject and interest to the DB, does not submit a vetting and immediately returns a 402
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 402
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: 1615rlso6haadcvui5gpkh13brr8vsee
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "vetting_result": "reject"
}
```

#### [V1] Registration (with a reject client) of a previously unregistered subject does not save the subject and interest to the DB, does not submit a vetting and immediately returns a 402
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v1+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 402
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
x-request-id: m1593tll1n0svjkqr1usqi1qan0mbumg
```
* __Response body:__
```json
{
  "vetting_result": "reject"
}
```

#### [V2] Registration (with a maintenance client) of a previously unregistered subject does not save the subject and interest to the DB, does not submit a vetting and immediately returns a 503
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 503
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: ss43ioago6f0v1dq300fqasp76hov05a
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "vetting_result": "maintenance"
}
```

#### [V1] Registration (with a maintenance client) of a previously unregistered subject does not save the subject and interest to the DB, does not submit a vetting and immediately returns a 402
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v1+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 402
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
x-request-id: 9aab0sfenh3hq35neo94rvifqjbv62b4
```
* __Response body:__
```json
{
  "vetting_result": "maintenance"
}
```

#### [V2] Registration (with an inactive client) of a previously unregistered subject saves the subject and interest to the DB, does not submit a vetting and immediately returns a 202
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 202
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: fqtl6uhp0f16lq2i4fjbbn9n9i3ad9gq
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "vetting_result": "inactive",
  "subject_identifier": "d2a4ac2a-feeb-11e8-95ba-c48e8ff4a5b7"
}
```

#### [V1] Registration (with an inactive client) of a previously unregistered subject saves the subject and interest to the DB, does not submit a vetting and immediately returns a 202
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v1+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 202
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
x-request-id: as5citstpk94bslv9tr0md4lnhjpi75k
```
* __Response body:__
```json
{
  "vetting_result": "inactive",
  "subject_identifier": "8da36e34-19eb-11e9-bccf-c48e8ff5e241"
}
```

#### [V2] Registration (with a burn-in client) of a previously unregistered subject does not save the subject to the DB, submits a vetting and immediately returns a 203
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 203
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: a0t7475td1oud5tsejta35nhsec27edd
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "vetting_result": "burn_in"
}
```

#### [V1] Registration (with a burn-in client) of a previously unregistered subject does not save the subject to the DB, submits a vetting and immediately returns a 203
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v1+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 203
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
x-request-id: gnp1dt3j17abvdhb6s973s1ao5esvc99
```
* __Response body:__
```json
{
  "vetting_result": "burn_in"
}
```

#### [V2] Registration (with an active client) of a previously unregistered subject results in an identity verification hit due to no credential response received with an accompanying warning
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: 6qs93ui0d41k0e7g7lejd68l6j21sql8
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "warnings": [
    "Georgia Drivers License File Currently Unavailable"
  ],
  "vetting_result": "passed",
  "subject_identifier": "5cb57b9e-fe63-11e8-946f-c48e8ff4a5b7",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "date_of_birth": "1977-04-14",
    "credential": {
      "status": "active",
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "identity_verification_result": "no_response_check_id",
  "errors": []
}
```

#### [V2] Registration (with an active client) of a previously unregistered subject results in an identity verification hit due to no credential response received
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: shdpfd995vl7jnc8p0m639i9gc3vekg0
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "warnings": [],
  "vetting_result": "passed",
  "subject_identifier": "5c8a197c-fe63-11e8-bab3-c48e8ff4a5b7",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "date_of_birth": "1977-04-14",
    "credential": {
      "status": "active",
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "identity_verification_result": "no_response_check_id",
  "errors": []
}
```

#### [V2] Registration (with an active client) of a previously unregistered subject results in an identity verification inconclusive
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: tocqu75ap0vr0rmnsjtm8ac4745ht93s
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "warnings": [],
  "vetting_result": "passed",
  "subject_identifier": "5caa377a-fe63-11e8-be18-c48e8ff4a5b7",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "date_of_birth": "1977-04-14",
    "credential": {
      "status": "active",
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "identity_verification_result": "inconclusive",
  "errors": []
}
```

#### [V2] Registration (with an active client) of a previously unregistered subject results in an identity verification hit
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: Fa_H6tcZqv3pE5oAAAHi
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "warnings": [],
  "vetting_result": "passed",
  "subject_identifier": "5c9584ce-fe63-11e8-b683-c48e8ff4a5b7",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "date_of_birth": "1977-04-14",
    "credential": {
      "status": "active",
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "identity_verification_result": "failed",
  "errors": []
}
```

#### [V2] Registration (with an active client) of a previously unregistered subject results in warning
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: kgo32tac5dn61j0m13rl1jnmn9d2a5t1
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "warnings": [
    "VA not accepting DQ - will not forward"
  ],
  "vetting_result": "passed",
  "subject_identifier": "cfcc9954-1e58-11e9-838e-c48e8ff5e241",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "date_of_birth": "1977-04-14",
    "credential": {
      "status": "active",
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "identity_verification_result": "passed",
  "errors": []
}
```

#### [V2] Registration (with an active client) of a previously unregistered subject results in errors
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: vm2b4vcu5a434g7nv9m0bqr5jvo4cg9e
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "warnings": [],
  "vetting_result": "error",
  "subject_identifier": "a2bc1dc6-fe5b-11e8-95eb-c48e8ff4a5b7",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "date_of_birth": "1977-04-14",
    "credential": {
      "status": "active",
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "identity_verification_result": "passed",
  "errors": [
    "Message Rejected - Invalid User ID"
  ]
}
```

#### [V2] Registration (with an active client) of a previously unregistered subject results in an NCIC alert and no errors
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: bun60mk6unljtacort70lqbi3opj45lt
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "warnings": [],
  "vetting_result": "failed",
  "subject_identifier": "0b8d2850-89f8-11e8-be24-c48e8ff4a5b7",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "date_of_birth": "1977-04-14",
    "credential": {
      "status": "active",
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "identity_verification_result": "passed",
  "errors": []
}
```

#### [V2] Registration (with an active client) of a previously unregistered subject results in no alerts or errors
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: oihs7q6qnevds65bf1jfr3vambensvp7
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "warnings": [],
  "vetting_result": "passed",
  "subject_identifier": "0b8266cc-89f8-11e8-a79e-c48e8ff4a5b7",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "date_of_birth": "1977-04-14",
    "credential": {
      "status": "active",
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "identity_verification_result": "passed",
  "errors": []
}
```

#### [V1] Registration (with an active client) of a previously unregistered subject results in errors
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v1+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 500
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
x-request-id: 4vi5c2lj4agcq6pcc90ng10c8sjo1qev
```
* __Response body:__
```json
{
  "error": {
    "message": "Message Rejected - Invalid User ID",
    "details": [
      "Message Rejected - Invalid User ID"
    ]
  }
}
```

#### [V1] Registration (with an active client) of a previously unregistered subject results in an NCIC alert
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v1+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
x-request-id: 07k90tr3oo248np9eeouv99d0mmpp1m5
```
* __Response body:__
```json
{
  "wanted": false,
  "vetting_result": "failed",
  "suspected_terrorist": false,
  "subject_identifier": "8dac234e-19eb-11e9-83a2-c48e8ff5e241",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "passport_number": null,
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "drivers_license_state": "TX",
    "drivers_license_number": "10287864",
    "date_of_birth": "1977-04-14",
    "credential": {
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "alerts": [
    {
      "wanted": false,
      "uri": "/api/alerts/3e0cbde6-29a5-11e6-9e68-000000000000",
      "reason": "",
      "overridden_at": null,
      "identifier": "3e0cbde6-29a5-11e6-9e68-000000000000",
      "id": null,
      "alert_type": "ncic"
    }
  ]
}
```

#### [V1] Registration (with an active client) of a previously unregistered subject results in no alerts
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v1+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 201
* __Response headers:__
```
content-type: application/json; charset=utf-8
cache-control: max-age=0, private, must-revalidate
x-request-id: 25r3aqrsvlrho2h2fbh26c8hs80kkule
```
* __Response body:__
```json
{
  "wanted": false,
  "vetting_result": "passed",
  "suspected_terrorist": false,
  "subject_identifier": "8d721974-19eb-11e9-ab18-c48e8ff5e241",
  "subject_attributes": {
    "weight": 215,
    "ssn": "123456789",
    "race": "WHITE",
    "passport_number": null,
    "name_suffix": null,
    "name_middle": "KEITH",
    "name_last": "MATTERN",
    "name_first": "JEREMY",
    "image_as_base64": "/9j/4AAQSkZJRgABAAAAAQABA ...",
    "home_address_zip_code": "77002",
    "home_address_street": "5300 Memorial Dr., Suite 940",
    "home_address_state_or_province": "TX",
    "home_address_city": "Houston",
    "height": "6-01",
    "hair_color": "BROWN",
    "gender": "MALE",
    "eye_color": "BROWN",
    "drivers_license_state": "TX",
    "drivers_license_number": "10287864",
    "date_of_birth": "1977-04-14",
    "credential": {
      "state": "TX",
      "number": "10287864",
      "issued_on": null,
      "identifier": "TX10287864",
      "expires_on": "2022-04-14",
      "credential_type": "state_id_or_dl",
      "card_type": "DL"
    }
  },
  "alerts": []
}
```

