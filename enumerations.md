# Enumerations

## CredentialStatus

* `active`     - The credential is active and valid
* `expired`    - The credential is expired
* `inactive`   - The credential is not active
* `revoked`    - The credential has been revoked
* `suspended`  - The credential has been suspended
* `unknown`    - The credential is in an unkown state (usually due to missing data or an unrecognized status from the state)
* `unlicensed` - The subject is unlicensed (likely a state ID)

## IdentityVerificationResult

* `failed`               - The identity verification results passed
* `inconclusive`         - The identity verification results were inconclsuive (usually due to a lack of data)
* `no_response_check_id` - Due to no DMV response the credential should be manually inspected for validity
* `passed`               - The identity verification results passed

## VettingResult

* `burn_in`      - The clients status is in burn_in - used to submit live data to state switches without rendering a decision
* `error`        - The subject vetting resulted in an error (check the errors array)
* `failed`       - The subject failed the vetting
* `inactive`     - The client status is inactive (DEPRECATED)
* `inconclusive` - The subject vetting resulted in no decision (usually due to lack of data)
* `maintenance`  - NCITE Vetting is in maintenance mode (usually due to a software deployment, etc)
* `passed`       - The subject passed the vetting
* `reject`       - The client status is in reject - used to turn off clients for numerous business reasons
* `state_down`   - The state switch is returning no responses
