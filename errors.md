# API Errors

* [NciteVetting.Frontend.Leidos.RegistrationController](#ncitevettingfrontendleidosregistrationcontroller)
  * [create](#ncitevettingfrontendleidosregistrationcontrollercreate)

## NciteVetting.Frontend.Leidos.RegistrationController
### NciteVetting.Frontend.Leidos.RegistrationController.create
#### [V2] Registration (with an active client) results in unhandled error
##### Request
* __Method:__ POST
* __Path:__ /api/leidos/registrations
* __Request headers:__
```
content-type: application/json
accept: application/vnd.ncite-vetting.v2+json
x-ssl-client-verified: SUCCESS
x-ssl-client-s-dn: C=US, ST=VA, L=Fort Belvoir, O=US Army, CN=belvoir.army.mil
x-ssl-client-cert: MIIEbTCCAlUCAQEwDQYJKoZIhvcNAQEFBQAwgZoxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJUWDEQMA4GA1UEBwwHSG91c3RvbjEUMBIGA1UECgwLSWJlcm9uLCBMTEMxITAfBgNVBAsMGExhdyBFbmZvcmNlbWVudCBEaXZpc2lvbjETMBEGA1UEAwwKaWJlcm9uLmNvbTEeMBwGCSqGSIb3DQEJARYPaW5mb0BpYmVyb24uY29tMB4XDTE1MDcxNDE5MjgzM1oXDTI1MDcxMTE5MjgzM1owXjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlZBMRUwEwYDVQQHEwxGb3J0IEJlbHZvaXIxEDAOBgNVBAoTB1VTIEFybXkxGTAXBgNVBAMTEGJlbHZvaXIuYXJteS5taWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHSmVgSxyEWaClrmZszDRTLLyAnHTn3N3oIYXH6wVQJnnxXVomFH85ErP5+0/Kip8SpVliW1f8fEjDprme0p/NMNeQtPyYswLTwb6ksS5fGD8IGxi384YPhxLyP9mne2DYLR3QYb0t9jFzejOaaK1wGeXnSz9IQCbE0cMOlJIHnLacuUiS7X8hC+k9dERvtpaHC2IVekH+6NtUoT4qlUCDC8SinIg2OqDFOPwMMeaJ3ykDvZxj69K+re1fUUepT3l7DCphHmGNT4DNDx1W9fB9Orzg+qgP/llV5BiOJVQsg91hGhet9RY9+u9jND2ZvmEREm5Sit2JU0LKmkpg9X01AgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA821jBf3f+AtV55i8Mhr2CfIlBEy2O+p1h5t/mL7UCBva2GiPfLipSwz61vLBQ17NXpvVjvktJyCLUXvho0/WtFEZ34o86iGun9boyakmTe379KqQO2rPuAXk99Slpagy22Vcv6BtgBWd9IBTGKPEFA/Fml5KJWK1PpcpNgXMPG7Q0cIF/eYTk0ZvdzQLZNU+iisxcVpuqV0uk6gyOmxfB/brwtaiikTZn1/UDNzrugQ8C41hhea6CoB6+cEx/mY/1vhpDD3yyalWI6ig/U5JedFSlXBP0TZzEIEg3vZLVLhtJ3mAV2gEd5ppCecbJ2rCsrArZyC4YWouL7u4zABe8tb/75cDMKpckzFC+11Oa13pmOIyQxsHcEB/9o/MkWwLVDmuCX6olgNaykjzAkSZh3mh4GDVhU5/ckzfRl4B58bxmpVxJ+cS0d6F5x6LANNdRJhZK5v9HgsFUpv1munwj44VcRLS6H4eEqSlpRvh/t19CRRoV2FvgodxaQVzt2kHf6sr5U+MRWckMK6uwlLlG/sGE0Z5tApjy+rbLg4i3lwJfBqtfO0LuawD7whOKZt/SJur/f455LDgvkiiQhDMw1C3pQeI1XdRHyzjkMitGtsEeGbCAB2V6QsdSYI/x0blFNnmTtrMlV5xcU22yomcea0o2fjOQ+OS+J6tJ3AMJ9
x-ncite-usr-email: john.smith@belvoir.army.mil
x-ncite-usr-name: John Smith
x-ncite-usr: ABC123
x-ncite-hostname: vrh-spc
x-ncite-dac: SVX2
x-ncite-device-id: 12345678901234
x-ncite-ori: TX10121X2
x-ncite-usr-nlets-login: ABC123
```
* __Request body:__
```json
{
  "vetting_cycle_in_days": 30,
  "ssn": "987654321",
  "race": "white",
  "pass_type": "visitor",
  "name_middle": "Keith",
  "name_last": "Mattern",
  "name_first": "Jeremy",
  "gender": "male",
  "email": "jmattern@iberon.com",
  "edipi": "1234567890",
  "drivers_license_state": "TX",
  "drivers_license_number": "123456789",
  "date_of_birth": "1977-04-14"
}
```
##### Response
* __Status__: 500
* __Response headers:__
```
cache-control: max-age=0, private, must-revalidate
x-request-id: 1615rlso6haadcvui5gpkh13brr8vsee
content-type: application/vnd.ncite-vetting.v2+json; charset=utf-8
```
* __Response body:__
```json
{
  "error": {
    "identifier": "edcd59ea-a29a-11e9-8d00-c48e8ff5e241",
    "message": "An error occurred"
  }
}
```

